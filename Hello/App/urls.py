from django.contrib import admin
from django.urls import path
from App import views

urlpatterns = [
    path("", views.index, name="Home"),
    path("Connect4/", views.Connect4, name="games"),
    path("TicTac/", views.TicTac, name="games"),
    path("Profile/", views.Profile, name="games")
    
]
