from django.shortcuts import render, HttpResponse

# Create your views here.
def index(request):
    return render(request, "Home.html")

def Connect4(request):
    return render(request, "Connect4.html")

def TicTac(request):
    return render(request, "TicTacToe.html")

def Profile(request):
    return render(request, "Profile.html")